compilar:limpiar
	mkdir bin
	find src -name "*.java" | xargs javac -cp bin -d bin
ejecutar:compilar
	java -cp bin Principal
limpiar:
	rm -rf bin
ver:crear
	java -jar p43.jar
crear:compilar
	jar cvfm p43.jar Manifest.txt -C bin/ .
javadoc:
	find . -type f -name "*.java" | xargs javadoc -d html -encoding utf-8 -docencoding utf-8 -charset utf-8
