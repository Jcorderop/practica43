package dominio;

import java.util.ArrayList;

public class Municipio {

    private String nombre;
    private ArrayList<Localidad> coleccionLocalidades;

    /**
     * Constructor de la clase
     * @param nombre nombre del municipio
     */
    public Municipio(String nombre) {
        this.nombre = nombre;
        coleccionLocalidades = new ArrayList<>();
    }

    /**
     * Método para obtener el nombre de la provincia
     * @return devuelve el nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo para modificar el nombre de la provincia
     * @param nombre setea un nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Método para añadir una localidad
     * @param localidad Localidad a añadir
     */
    public void addLocalidad(Localidad localidad) {
        coleccionLocalidades.add(localidad);
    }

    /**
     * Calcula el número de habitantes
     * @return devuelve el número de habitantes
     */
    public int calcularNumeroHabitantes() {
        int numeroHabitantes = 0;
        for (int i = 0; i < coleccionLocalidades.size(); i++){
            numeroHabitantes += (coleccionLocalidades.get(i)).getNumeroDeHabitantes();
        }
        return numeroHabitantes;
    }

    /**
     * Método para convertir en string la información de la clase
     * @return devuelve la información en forma de string
     */
    //Mi toString()

    /*public String toString() {
        return "\nMunicipio: "+ "Nombre: " + nombre + ". Numero de habitantes= " + calcularNumeroHabitantes()+ " \nLocalidades= " + coleccionLocalidades;
    }*/

    //El toString() del profesor
    public String toString(){
        String datos = "Municipio, " + nombre + " con " +
                calcularNumeroHabitantes() + " habitantes con las siguientes localidades: ";
        for (Localidad loc : coleccionLocalidades){
            datos += "\n" + loc;
        }
        return datos;
    }
}
