package dominio;

import java.util.ArrayList;

public class Provincia {

    private String nombre;
    private ArrayList<Municipio> coleccionMunicipios;

    /**
     * Constructor de la clase
     * @param nombre nombre de la provincia
     */
    public Provincia(String nombre) {
        this.nombre = nombre;
        coleccionMunicipios = new ArrayList<>();
    }

    /**
     * Calcúla el número de habitantes
     * @return devuelve el número de habitantes
     */
    public int calcularNumeroHabitantes() {
        int numeroHabitantes = 0;
        for (int i = 0; i < coleccionMunicipios.size(); i++){
            numeroHabitantes += (coleccionMunicipios.get(i)).calcularNumeroHabitantes();
        }
        return numeroHabitantes;
    }

    /**
     * Metodo para añadir un municipio
     * @param municipio Municipio a añadir
     */
    public void addMunicipio(Municipio municipio) {
        coleccionMunicipios.add(municipio);
    }

    /**
     * Método para obtener el nombre de la provincia
     * @return devuelve el nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo para modificar el nombre de la provincia
     * @param nombre setea un nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Método para convertir en string la información de la clase
     * @return devuelve la información en forma de string
     */
    //Mi toString()
    /*public String toString() {
        return "Provincia: " +"nombre: " + nombre + ". Numero de habitantes=" + calcularNumeroHabitantes() + " \nMunicipios= " + coleccionMunicipios;
    }*/

    //El toString() del profesor

    public String toString(){
        String datos = "Provincia, " + nombre + " con " +
                calcularNumeroHabitantes() + " habitantes con los siguientes municipios";
        for (Municipio municipio : coleccionMunicipios){
            datos += "\n" + municipio;
        }
        return datos;
    }
}
