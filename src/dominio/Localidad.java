package dominio;

public class Localidad {
    private String nombre;
    private int numeroDeHabitantes;

    /**
     * Constructor de la clase
     * @param nombre nombre de la localidad
     * @param numeroDeHabitantes numero de habitantes de la localidad
     */
    public Localidad(String nombre, int numeroDeHabitantes) {
        this.nombre = nombre;
        this.numeroDeHabitantes = numeroDeHabitantes;
    }

    /**
     * Método para obtener el nombre de la provincia
     * @return devuelve el nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo para modificar el nombre de la provincia
     * @param nombre setea un nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Método para obtener el número de habitantes
     * @return devuelve el número de habitantes
     */
    public int getNumeroDeHabitantes() {
        return numeroDeHabitantes;
    }

    /**
     * Método para modificar el número de habitantes
     * @param numeroDeHabitantes setea un número de habitantes
     */
    public void setNumeroDeHabitantes(int numeroDeHabitantes) {
        this.numeroDeHabitantes = numeroDeHabitantes;
    }

    /**
     * Método para convertir en string la información de la clase
     * @return devuelve la información en forma de string
     */
    //Mi toString()
   /* public String toString() {
        return "\nLocalidad: " + "Nombre: " + nombre + ". Número de habitantes= " + numeroDeHabitantes;
    }*/

    //El toString() del profesor
    public String toString(){
        return "Localidad, " + nombre + " con " + numeroDeHabitantes + " habitantes.";
    }
}

