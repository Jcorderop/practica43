package aplicacion;

import dominio.*;

//Así lo hago yo

/*public class Principal {
    public static void  main(String[] args){


        Localidad La_Moraleja = new Localidad("La Moraleja", 1000);
        Localidad Fuente_Hito = new Localidad("Fuente Hito", 850);
        Localidad Cuesta_Blanca = new Localidad("Cuesta Blanca", 500);

        Municipio alcobendas = new Municipio("Alcobendas");
        alcobendas.addLocalidad(La_Moraleja);
        alcobendas.addLocalidad(Fuente_Hito);
        alcobendas.addLocalidad(Cuesta_Blanca);

        Localidad Pozuelo_Centro = new Localidad("Pozuelo Centro", 1100);
        Localidad Pozuelo_Estacion = new Localidad("Pozuelo Estación", 800);
        Localidad Urbanizaciones  = new Localidad("Urbanizaciones ", 2500);

        Municipio alcorcon = new Municipio("Alcorcón");
        alcorcon.addLocalidad(Pozuelo_Centro);
        alcorcon.addLocalidad(Pozuelo_Estacion);
        alcorcon.addLocalidad(Urbanizaciones);

        Provincia madrid = new Provincia("Comunidad de Madrid");

        madrid.addMunicipio(alcobendas);
        madrid.addMunicipio(alcorcon);

        System.out.println(madrid);*/

// Así lo hace el profesor

        public class Principal{
            public static void main(String[] args){
                String[]  nombre_municipios = {"Alcorcón", "Alcobendas"};
                String[] nombre_localidades = {"Alcorón centro", "Alcorcón estación", "Alcorcoón McDonals", "Alcorcon Centro", "Alcobendas estación", "Alcobendas Burguer King"};
                int[] habitantes_localidades = {1000, 2000, 1500, 1250, 1500, 2300};

                Provincia provincia = new Provincia("Madrid");

                for (int i = 0; i < nombre_municipios.length; i++){
                    Municipio mun = new Municipio(nombre_municipios[1]);

                    for (int j = i * 3; j < (i+1)*3;j++){
                        Localidad loc = new Localidad(nombre_localidades[j], habitantes_localidades[j]);
                        mun.addLocalidad(loc);
            }
            provincia.addMunicipio(mun);
        }
                System.out.println(provincia);
    }
}
